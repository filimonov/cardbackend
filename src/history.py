"""Компонент истории."""

from collections import defaultdict
from datetime import datetime
from decimal import Decimal


class CommonLog(object):
    """Суперкласс логов."""

    def __init__(
        self,
        card_number: str,
        before: any,
        after: any,
        changes: any,
    ):
        self.card_number: str = card_number
        self.before: any = before
        self.after: any = after
        self.changes: any = changes
        self._datetime_utc: datetime = datetime.utcnow()


class BalanceLog(CommonLog):
    """Логи баланса."""

    def __init__(
        self,
        card_number: str,
        before: Decimal,
        after: Decimal,
        changes: Decimal,
    ):
        super().__init__(card_number, before, after, changes)


class LogStorage(object):
    """Хранилище логов."""

    def __init__(self):
        self._balance_logs: defaultdict[
            str, list[BalanceLog],
        ] = defaultdict(list)
        self._other_logs: list[CommonLog] = []

    def save(self, log: CommonLog):
        """Сохраняет логи в соответствующие хранилища."""
        if isinstance(log, BalanceLog):
            self._balance_logs[log.card_number].append(log)
        else:
            self._other_logs.append(log)

    def get_balance_history(
        self,
        card_number: str,
        since: datetime,
        to: datetime,
    ) -> list[BalanceLog]:
        """Возвращает историю баланса за указанный период."""
        return [
            log
            for log in self._balance_logs.get(card_number, [])
            if since <= log._datetime_utc <= to
        ]
