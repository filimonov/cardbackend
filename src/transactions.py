"""Компонент транзакций."""

from dataclasses import dataclass
from decimal import Decimal

from history import BalanceLog, LogStorage


@dataclass
class User(object):
    """Класс пользователя."""

    card_number: str
    limit: Decimal = 5000
    user_info: dict = None

    _balance: Decimal = 0

    @property
    def balance(self):
        """Доступ к балансу."""
        return self._balance

    @balance.setter
    def balance(self, new_balance):
        if new_balance > -self.limit:
            self._balance = new_balance
        else:
            raise ValueError('You have exceeded the withdrawal limit')


class UserStorage(object):
    """Хранилище пользователей."""

    def __init__(self):
        self._active: dict[str, User] = {}
        self._closed: list[str, User] = []

    def add(self, card_number: str, user_info: dict):
        """Добавить пользователя."""
        if card_number in self._active or card_number in self._closed:
            raise ValueError('User already exists')
        new_user = User(card_number=card_number, user_info=user_info)
        new_user.balance = Decimal(0)
        self._active[card_number] = new_user

    def get_user(self, card_number: str) -> User:
        """Найти пользователя."""
        return self._active.get(card_number)

    def update_user(self, user: User):
        """Обновить пользователя."""
        active_user = self._active.get(user.card_number)
        if active_user is not None:
            self._active[user.card_number] = user
        else:
            raise ValueError('User is not found')

    def close(self, card_number: str):
        """Закрыть пользователя."""
        active_user = self._active.pop(card_number)
        if active_user is not None:
            self._closed.append((card_number, active_user))
        else:
            raise ValueError('User is not found')


class Transactions(object):
    """Операции с кредитной картой."""

    def __init__(self, user_storage: UserStorage, history: LogStorage):
        self._user_storage: UserStorage = user_storage
        self._history: LogStorage = history

    def get_balance(self, card_number: str) -> Decimal:
        """Получить баланс карты."""
        current_user = self._user_storage.get_user(card_number)
        if current_user is not None:
            return current_user.balance

    def withdrawal(self, card_number: str, amount: Decimal):
        """Снятие средств с карты."""
        self._change_balance(card_number, -self._check_amount(amount))

    def deposit(self, card_number: str, amount: Decimal):
        """Пополнение средств карты."""
        self._change_balance(card_number, self._check_amount(amount))

    def update_info(self, card_number: str, new_info: dict):
        """Обновление информации о пользователе."""
        current_user = self._user_storage.get_user(card_number)
        if current_user is not None:
            current_user.user_info = new_info

    def change_limit(self, card_number: str, new_limit: Decimal):
        """Изменить лимит карты."""
        current_user = self._user_storage.get_user(card_number)
        if current_user is not None:
            current_user.limit = new_limit

    def _change_balance(self, card_number: str, amount: Decimal):
        current_user = self._user_storage.get_user(card_number)
        if current_user is not None:
            old_balance = current_user.balance
            new_balance = old_balance + amount
            log = BalanceLog(card_number, old_balance, new_balance, amount)
            self._history.save(log)
            current_user.balance = new_balance

    def _check_amount(self, amount: Decimal):
        if amount > 0:
            return amount
        raise ValueError('Amount should be a positive number')
